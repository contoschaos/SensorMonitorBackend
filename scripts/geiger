#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
geiger_basic.py - to collect data from Geiger Counter 
(it was originaly ment for GQ GMC-300-plus)
updated for 'GQ GMC-600 plus' 
with option to log data
using serial communication via /dev/ttyUSB0 (linked to /dev/ttyUSB0 )
"""

# usage: /path/to/geiger_basic.py
#
# device command coding taken from:  https://sourceforge.net/projects/ttyUSB0/
#
# To avoid having to run a script as root (required for write access to USB) it is suggested
# to define a udev rule. Then the script can be used as a regular user:
#
# As root create a file '52-ttyUSB0.rules' in '/etc/udev/rules.d' with this content:
#
#   SUBSYSTEM=="usb", ATTR{idVendor}=="1a86", ATTRS{idProduct}=="7523", MODE:="666", GROUP="plugdev"
#   SUBSYSTEM=="tty", KERNEL=="ttyUSB*", ATTRS{idVendor}=="1a86", MODE:="666", SYMLINK+="ttyUSB0"
#
# then do "sudo udevadm control --reload-rules" or restart your computer
# then unplug and replug your device
# this works on  Ubuntu Mate 16.04.01 with kernel 4.4.0-57-generic

# All possible commands (probably old)
# GETVER, GETSERIAL, GETCPM, KEY0, KEY1, KEY2, KEY3, SPEAKER0, SPEAKER1,
# ALARM0, ALARM1, GETVOLT, GETGYRO, GETCFG, GETCPS, GETTEMP, HEARTBEAT0,
# HEARTBEAT1, GETDATETIME, CFGUPDATE, POWEROFF, POWERON, SETDATETIME,
# FACTORYRESET
# https://www.gqelectronicsllc.com/download/GQ-RFC1201.txt

__author__      = "ullix"
__copyright__   = "Copyright 2016"
__credits__     = ["Phil Gillaspy"]
__license__     = "GPL"
__version__     = "0.0.2"
__maintainer__  = ""
__email__       = ""
__status__      = "Development"

import serial
import sys
import signal

byteType = "big"

def timeout_handler(signum, frame):
    raise TimeoutError("Timed out!")

# In contrast 300 -plus this read one additional bytes,
# because this is ment for 600-plus
def getCPM(ser):
    # send <GETCPM>> and read 4 bytes; interpret as MSB and LSB

    ser.write(b'<GETCPM>>')
    rec = ser.read(4)

    return int.from_bytes(rec, byteType)

def getCPS(ser):
    # send <GETCPs>> and read 4 bytes; interpret as MSB and LSB

    ser.write(b'<GETCPS>>')
    rec = ser.read(4)

    return int.from_bytes(rec, byteType)


#Not available on GMC-600-plus model
def getTEMP(ser):
    # Firmware supported: GMC-320 Re.3.01 or later (not 600-plus)
    # send <GETTEMP>> and read 4 bytes
    # Return: Four bytes celsius degree data in hexdecimal: BYTE1,BYTE2,BYTE3,BYTE4
	# Here: BYTE1 is the integer part of the temperature.
	#       BYTE2 is the decimal part of the temperature.
	#       BYTE3 is the negative signe if it is not 0.  If this byte is 0, the then current temperture is greater than 0, otherwise the temperature is below 0.
	#       BYTE4 always 0xAA

    ser.write(b'<GETTEMP>>')
    rec = ser.read(4)

    return int.from_bytes(rec, byteType)

#Not tested for GMC-600-plus
def getGYRO(ser):
    # Firmware supported: GMC-320 Re.3.01 or later
    # Send <GETGYRO>> and read 7 bytes
    # Return: Seven bytes gyroscope data in hexdecimal: BYTE1,BYTE2,BYTE3,BYTE4,BYTE5,BYTE6,BYTE7
	# Here: BYTE1,BYTE2 are the X position data in 16 bits value. The first byte is MSB byte data and second byte is LSB byte data.
	#       BYTE3,BYTE4 are the Y position data in 16 bits value. The first byte is MSB byte data and second byte is LSB byte data.
	#       BYTE5,BYTE6 are the Z position data in 16 bits value. The first byte is MSB byte data and second byte is LSB byte data.
	#       BYTE7 always 0xAA

    ser.write(b'<GETGYRO>>')
    rec = ser.read(7) # 1 byte is always empty (0)

    x=int.from_bytes(rec[0:1], byteType)
    y=int.from_bytes(rec[2:3], byteType)
    z=int.from_bytes(rec[4:5], byteType)
    gyro = "X:{};Y:{};Z:{}".format(x,y,z)

    return gyro

def getCFG(ser):
    # In doumentation it said 256, but 512 can be read

    ser.write(b'<GETCFG>>')
    rec = ser.read(512)
    
    # Convert byte data to hexadecimal representation
    hex_representation = ' '.join(f'{byte:02X}' for byte in rec)

    return hex_representation

def getAlertLevel(cpm):
    if(cpm > 95):
        return 2, "High CPM count!"
    elif(cpm > 75):
        return 1, "Above average ambient levels."
    return None

#Uses usb port, so usb driver cannot be disabled for less power consumed
def getData():
    # Set a timeout of 10 seconds
    signal.signal(signal.SIGALRM, timeout_handler)
    signal.alarm(10)
    
    # open the serial port !!! 
    # NOTE: baud rate must be set at device first, default is 57600 !!!
    # TODO asign not static port
    try:
        ser = serial.Serial('/dev/ttyUSB0', 115200)
        try:
            #(Calculations can be found/digged up from USB recieved with your 600-plus )
            cpm = getCPM(ser)
            return '"3": {{"n": "{}"}}'.format(cpm), getAlertLevel(cpm)
        except KeyboardInterrupt: #TODO test if can be removed
            pass
        finally:
            ser.close()
    except TimeoutError as error:
        return None, (0, "Timeout")
    except serial.SerialException as e:
        return None, (0, "Exeption")
    finally:
        signal.alarm(0)
    
#For testing puroses, usefull to debug in rasspbery if everything works properly
if(len(sys.argv) >= 2 and sys.argv[1] == 'test'):
    print(getData())