#!/usr/bin/python
import sys
import RPi.GPIO as gpio
import time
import multiprocessing

BUZZ_PIN = 22 # Physical pin number 15

def alert(times):
    """
    Function which uses PWM on BUZZ_PIN to signalize an alert.
    ( Connect piezo buzzer or LED to specified BUZZ_PIN ) 
    """
    try:
        gpio.setmode(gpio.BCM)
        gpio.setup(BUZZ_PIN, gpio.OUT)
        gpio.setwarnings(False)
        pwm = gpio.PWM(BUZZ_PIN,6000)
        pwm.start(25)
        for i in range(0,1000 * times):
            if(i%2 > 0):
                #pwm.ChangeDutyCycle(i)
                pwm.ChangeFrequency(12000)
            else:
                #pwm.ChangeDutyCycle(i)
                pwm.ChangeFrequency(6000)
            time.sleep(0.01)
    finally:
        gpio.cleanup()

def run():
    """
    Start async process which uses BUZZ_PIN of raspbery to signalize an alert.
    """
    process = multiprocessing.Process(target=alert,args=[1])
    process.start()

def runLong():
    """
    Start async process which uses BUZZ_PIN of raspbery to signalize an alert for longer time.
    """
    process = multiprocessing.Process(target=alert,args=[3])
    process.start()

if(len(sys.argv) >= 2 and sys.argv[1] == '--reboot'):
    runLong()