#!/usr/bin/python
from datetime import datetime
import importlib.machinery as importLib
import os
import alerter
import requests
import json
import resource
import signal

# 200 * 1024 * 1024 # 200 MB max memory
max_mem=209715200

# Set maximum memory limit
resource.setrlimit(resource.RLIMIT_AS, (max_mem, max_mem))

url = 'http://localhost:8752/current_samplers'

def timeout_handler(signum, frame):
    raise TimeoutError("Timed out!")

def importScriptAndRun(name):
    """
    Runs python script form 'scripts' folder calling getData().
    Calls alerter, when sensor script returns the alert level higher than 1.
    Args:
        name (string): name of script

    Returns:
        string: JSON string data from sensor script
    """
    if(name == "__pycache__"):
        return None

    # Import mymodule
    loader = importLib.SourceFileLoader( name, 'scripts/'+name )
    data, alert = loader.load_module().getData()

    if(alert != None and alert[0]>1):
        print(alert)
        alerter.run()

    return data

#Start
dataToWrite = ''
now = datetime.now()
date = now.strftime("%d-%m-%Y")
time = now.strftime("%H-%M")

# Set a timeout of 58 seconds
signal.signal(signal.SIGALRM, timeout_handler)
signal.alarm(58)

try:
    # Run scripts for sensors, that don't require waiting for sampling.
    for i,scriptName in enumerate(os.listdir('./scripts')):
        result=importScriptAndRun(scriptName)
        if(result != None):
            dataToWrite='{}{},'.format(dataToWrite,result)

    # Call server for sensors, that require waiting for sampling.
    try:
        response = requests.get(url)
        # Check the status code of the response
        if response.status_code == 200 and response.text != '' and response.text != '""':
            # Request was successful, print the response content
            dataToWrite='{}{},'.format(dataToWrite,json.loads(response.text)[1:-2])
    except requests.exceptions.ConnectionError as e:
        print("connection err")

    #data file, append mode
    fileLog = open("./data/"+date, "a")
    #fileCurrent = open("./data/current","w")
    if(dataToWrite!=''):
        l = len(dataToWrite)
        dataToWrite=dataToWrite[:l-1]
        #fileCurrent.write('{{"data":[{}]}}\n'.format(dataToWrite))
        fileLog.write('"{}":{{{}}}\n'.format(time,dataToWrite))
    fileLog.close()
except TimeoutError as error:
    print("Timeout")
finally:
    signal.alarm(0)  
