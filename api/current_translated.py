from dataclasses import dataclass
from email import message
from types import SimpleNamespace
import json
import currentCall
import sys

TAB_SIZE = 13

def appendAllInArrayToDict(dictionary, arr):
    """
    Args:
        dictionary (dict): any dict to which entrys will be appened
        arr (array): array with keypairs to be added
    Returns:
        string: updated dictionary
    """
    res = dictionary
    for i in range(len(arr)):
        dictionary.update(arr[i])
    return res

def getData(attDict,data):
    """
    For each attribute.
    Args:
        attDict (dict): dict coresponding to attributes
        data (array): data array to be translated
    Returns:
        string: formated string of all attributes
    """
    result = ''
    keys = list(data.keys())
    for i in range(len(keys)):
        result += '{} = {}; '.format(attDict[keys[i]], data[keys[i]])

    return result

def formatData(itemData):
    keyList = list(itemData.keys())
    key = keyList[0]
    tab = ' ' * (TAB_SIZE - len(sensorDict[key]))
    dataString = '{}{}: {}'.format(sensorDict[key], tab, getData(attDict,itemData[key]))+'<br>'
    if(len(keyList) > 1):
        dataString += '[{},{}]<br>'.format(itemData['lvl'],itemData['msg'])
    return dataString
    

#Start
resJSON = currentCall.run(False,None)
data = json.loads(resJSON)['data']

sensorDict, attDict = {},{}
dictionaryRaw = json.loads(open('dictionary.json').read())
sensorDict = appendAllInArrayToDict(sensorDict,dictionaryRaw['sensors'])
attDict = appendAllInArrayToDict(attDict,dictionaryRaw['attributes'])
toPrint = ''

#For each sensor
for i in range(len(data)):
    toPrint += formatData(data[i])

if(len(sys.argv) >= 2):
    for item in sys.argv[1:]:
        if(item == ''):
            continue
        toPrint += formatData(json.loads(item))
        
print(toPrint)