from datetime import datetime
import importlib.machinery as importLib
import os

SCRIPTS_FOLDER='/home/pi/pi/scripts/'
SAMPLER_FOLDER='/home/pi/pi/scriptsSampling/'

def importScriptAndRun(name,folder):
    """ Runs python script form /script folder calling getData().
    Args:
        name (string): name of script

    Returns:
        string: JSON string data from sensor script
    """
    if(name !="__pycache__"):
        # Import mymodule
        loader = importLib.SourceFileLoader( name, folder + name )
        return loader.load_module().getData()

def run(printIt, scriptName):
    """ Runs script to get latest data. 
    Args:
        printIt (boolean): Print result to console or returns it.
    Returns:
        string: JSON string data from sensor script
    """
 
    dataToWrite = ''
    now = datetime.now()
    date = now.strftime("%d-%m-%Y")
    time = now.strftime("%H-%M")
    
    if (scriptName):
        result = importScriptAndRun(scriptName, SAMPLER_FOLDER)
        print(result)
        if(result[1] == None):
            dataToWrite='{}{{{}}},'.format(dataToWrite,result[0])
        else:
            dataToWrite = '{0}{{{1},"lvl":"{2}","msg":"{3}"}},'.format(dataToWrite,result[0],result[1][0],result[1][1])
    else:     
        for _,scriptName in enumerate(os.listdir(SCRIPTS_FOLDER)):
            result = importScriptAndRun(scriptName,SCRIPTS_FOLDER)
            if(result != None and result[0] != None):
                if(result[1] == None):
                    dataToWrite='{}{{{}}},'.format(dataToWrite,result[0])
                else:
                    dataToWrite='{0}{{{1},"lvl":"{2}","msg":"{3}"}},'.format(dataToWrite,result[0],result[1][0],result[1][1])


    if(dataToWrite!=''):
        l = len(dataToWrite)
        dataToWrite=dataToWrite[:l-1]
        res = '{{"data":[{}]}}\n'.format(dataToWrite)
        if(printIt):
            print(res)
        else:
            return res