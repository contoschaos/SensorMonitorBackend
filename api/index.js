const { json } = require('express');
const express = require('express')
const fs = require('fs');
const { PythonShell } = require('python-shell');
const { exec } = require('child_process');
const app = express()

//TODO add to ENV file
const port = 8752
const space = '/home/pi/pi'
const currentPy = space.concat('/api/current.py');
const currentSamplePy = space.concat('/api/currentSample.py');
const current_translated = space.concat('/api/current_translated.py');
const scriptsFolder = space.concat('/scripts/');
const samplersFolder = space.concat('/scriptsSampling/');
const dataFolder = space.concat('/data/');

var intervalObj = undefined;
var alive = false;

const samplersDataMap = new Map();

const SCRIPT = {
  START_TRANSCODE: 0,
  STOP_TRANSCODE: 1
}

function runScript(script) {
  let toRun = '';
  if (script === SCRIPT.START_TRANSCODE) {
    toRun = 'runTranscoder.sh';
  } else if (script === SCRIPT.STOP_TRANSCODE) {
    toRun = 'stopAllTranscoders.sh';
  } else {
    return;
  }

  exec('sh ../../scripts/'.concat(toRun),
    (error, stdout, stderr) => {
      console.log(stdout);
      console.log(stderr);
      if (error !== null) {
        console.log(`exec error: ${error}`);
      }
    });
}

function transcoderProcess() {
  if (!intervalObj) {
    // Start transcoders
    // Example without leaking personal info (<SOMETHING> = replace):
    // cvlc -I dummy -vvv rtsp://<USERNAME>:<PASSWORD>@<ADDRESS> --sout '#transcode{vcodec="theo",venc=theora{quality=3},vb="",scale="0.25"} :standard{mux=ogg,access=http,dst=":<PORT>"}'
    // Runs CVLC (vlc without gui) that takes stream and transcode it to output on given port. ( Don't forget about port settings ...)
    runScript(SCRIPT.START_TRANSCODE)

    // Set interval
    intervalObj = setInterval(() => {
      if (!alive) {
        // Stop All
        //EXAMPLE: pgrep -f vlc | xargs kill
        runScript(SCRIPT.STOP_TRANSCODE)
        clearInterval(intervalObj)
        intervalObj = undefined
      } else {
        // Continue and reset flag
        alive = false
      }
    }, 120000);
  } else {
    // Sets flag to keep transcoder running
    alive = true
  }
}

function readDirScripts(dirName, callback) {
  fs.readdir(dirName, (err, files) => {
    var filteredFiles = files.filter(file => file != '__pycache__')
    if (err) {
      const err_message = '{"error":"DataLoadingError"}'
      console.log(err_message)
      callback(err_message)
    } else {
      callback(filteredFiles)
    }
  })
}

let samplersSet=false;
function setupSamplers() {
  if(samplersSet){
    console.log("Samplers set"); // dodo use list of intervals and clear it ?
    return;
  }

  readDirScripts(samplersFolder, (scripts => {
    scripts.forEach(script => {
      setInterval(() => {
        PythonShell.run(currentSamplePy, { args: script },
          (err, result) => {
            if (err) {
              console.log(err)
              console.log('{"err":"SamplerSetupError"}')
              return
            } else {
              try {
                if (result[1].includes('None')) {
                  samplersDataMap.set(script, undefined)
                }
                else {
                  const response = JSON.parse(result[1]);
                  samplersDataMap.set(script, response.data[0])
                }
              } catch (err) {
                console.log("ERROR: Fail parse!")
                console.log(err)
              }
            }
          });
      }, 60000);
    });
  }))
  samplersSet=true;
}

//TODO - Config max requests for second (for example 5 a sec)

// **************************************************************************
// **************************************************************************

// Do once

setupSamplers()

// **************************************************************************
// **************************************************************************

/**
* Add headers before the routes are defined.
*/
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Pass to next layer of middleware
  next();
});

/**
 * Returns current data from all sensor scripts.
 * And start video transcoding (If not called in specified interval, then transcoders will stop.)
 */
app.get('/current', (req, res) => {
  // Transcoders stuff
  transcoderProcess()

  // Use python shell to get current data
  PythonShell.run(currentPy, null,
    (err, result) => {
      if (err) {
        console.error(err)
        res.json('{"err":"DataLoadingError"}')
        return
      }


      let samplersData = ','
      const appendIndex = result[0].length - 2
      if (appendIndex > 0) {
        samplersDataMap.forEach(value => {
          if (value) {
            samplersData = samplersData.concat(JSON.stringify(value), ',')
          }
        })

        samplersData = samplersData.slice(-1)

        res.json([
          result[0].slice(0, -2),
          samplersData.length > 1 ? samplersData : '',
          result[0].slice(appendIndex)
        ].join(''));
      }
    });
});

/**
 * Returns current data from all samplers sensor scripts.
 */
app.get('/current_samplers', (req, res) => {
  // Use python shell to get current data
  let samplersValues = ''

  if (samplersDataMap.size) {
    samplersDataMap.forEach(value => {
      if (value) {
        samplersValues = samplersValues.concat(JSON.stringify(value), ',')
      }
    })
  } else {
    console.error('No data yet!')
    res.json(undefined)
    return
  }
  res.json(samplersValues)
});

/**
 * Returns current data from all sensor scripts. 
 * In readable html site format.
 * TODO - append + translate sampler data too 
 */
app.get('/current_translated', (req, res) => {
  const data = [...(samplersDataMap.values())]
  const pyArguments = data.map((item) => item ? JSON.stringify(item) : '')

  console.log(pyArguments)
  // Use python shell
  PythonShell.run(current_translated, { args: pyArguments },
    (err, result) => {
      if (err) {
        console.error(err)
        res.status(500).send('Error.')
        return
      }
      res.type('html')
      res.send('<!DOCTYPE html>\
      <html><body>\
      <pre>'.concat(result, '</pre>\
      </body></html>'))
    });
});

/**
 * Date param expected format is dd-mm-yyyy.
 */
app.get('/history/:date', (req, res) => {
  const formatRegex = /^([0-3][0-9])-([0-1][0-9])-([0-9])+$/

  if (!req.params || !req.params.date || !req.params.date.match(formatRegex)) {
    console.error("FormatError")
    res.json('{"err":"FormatError"}')
    return
  }

  fs.readFile(dataFolder.concat(req.params.date), 'utf8', (err, data) => {
    if (err || !data) {
      console.error(err);
      res.json('{"err":"DataLoadingError"}')
      return
    }

    //formats data
    let regex = /\n/g; //each line elnd
    data = data.replaceAll(regex, ',\n'); //replace with ,
    data = data.slice(0, -2); //remove last
    res.json('{"data":{'.concat(data, '}}'));
  });
});

/**
 * Dictionary JSON is returned.
 */
app.get('/dictionary', (req, res) => {
  fs.readFile(space.concat('/api/dictionary.json'), 'utf8', (err, data) => {
    if (err || !data) {
      console.error(err);
      res.json('{"err":"DataLoadingError"}')
      return
    }

    let regex = /\n/g; //each line elnd
    data = data.replaceAll(regex, ''); //replace with ''
    res.json(data);
  });
});

/**
 * Test any script. Returns JSON response.
 */
app.get('/testRun/:script', (req, res) => {
  const formatRegex = /^[a-zA-Z0-9_]*$/
  if (!req.params || !req.params.script || !req.params.script.match(formatRegex)) {
    console.error("FormatError")
    res.json('{"err":"FormatError"}')
    return
  }

  PythonShell.run(scriptsFolder.concat(req.params.script), { args: 'test' }, (err, result) => {
    if (err) {
      console.error(err);
      res.json('{"err":"DataLoadingError"}')
      return
    }

    res.json(result);
  });
});

/**
 * Test any sampler script. Returns JSON response.
 */
app.get('/testRun/sampler/:script', (req, res) => {
  const formatRegex = /^[a-zA-Z0-9_]*$/
  if (!req.params || !req.params.script || !req.params.script.match(formatRegex)) {
    console.error("FormatError")
    res.json('{"err":"FormatError"}')
    return
  }

  PythonShell.run(samplersFolder.concat(req.params.script), { args: 'test' }, (err, result) => {
    if (err) {
      console.error(err);
      res.json('{"err":"DataLoadingError"}')
      return
    }

    res.json(result);
  });
});

/**
 * Returns JSON list of each script.
 */
app.get('/getScripts', (req, res) => {
  readDirScripts(scriptsFolder, (scripts => {
    res.json(scripts)
  }))
});

/**
 * Returns JSON list of each sampler script.
 */
app.get('/getScripts/sampler', (req, res) => {
  readDirScripts(samplersFolder, (scripts => {
    res.json(scripts)
  }))
});

/**
 * Get TEXT/code of any script in JSON format.
 */
app.get('/getScript/:script', (req, res) => {
  fs.readFile(scriptsFolder.concat(req.params.script), 'utf8', (err, data) => {
    if (err) {
      console.error(err)
      res.json('{"error":"DataLoadingError"}')
    }


    res.json(JSON.stringify(data))
  })
})

app.listen(port, () => {
  console.info(`Example app listening at http://localhost:${port}`)
});
