#!/usr/bin/python
import board
import sys
import signal
import adafruit_lis3mdl

#https://github.com/adafruit/Adafruit_CircuitPython_LIS3MDL/blob/aaf823bf7ccef45a4f0246dc79cc9dbbe08a3cc6/adafruit_lis3mdl.py#L107

i2c = board.I2C()  # uses board.SCL and board.SDA
sensor = adafruit_lis3mdl.LIS3MDL(i2c)

if(sensor.operation_mode == adafruit_lis3mdl.OperationMode.POWER_DOWN or sensor.operation_mode == adafruit_lis3mdl.OperationMode.CONTINUOUS):
    print(adafruit_lis3mdl.OperationMode.string[sensor.operation_mode])
    print('change')
    sensor.operation_mode = adafruit_lis3mdl.OperationMode.SINGLE
    #sensor.performance_mode = adafruit_lis3mdl.PerformanceMode.MODE_LOW_POWER

print(adafruit_lis3mdl.Rate.string[sensor.data_rate])
print(adafruit_lis3mdl.OperationMode.string[sensor.operation_mode])
print(adafruit_lis3mdl.PerformanceMode.string[sensor.performance_mode])
print(adafruit_lis3mdl.Range.string[sensor.range])

