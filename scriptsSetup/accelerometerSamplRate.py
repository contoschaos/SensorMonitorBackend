import time
import board
import adafruit_lis331

def write_value(shared_value, value):
    shared_value.value = value

start_time = time.time()
max_value_x = float('-inf')  # Initialize max_value to negative infinity
max_value_y = float('-inf')  # Initialize max_value to negative infinity
max_value_z = float('-inf')  # Initialize max_value to negative infinity

i2c = board.I2C()  # uses board.SCL and board.SDA
lis = adafruit_lis331.LIS331HH(i2c)

# use a nice fast data rate to for maximum resolution
lis.data_rate = adafruit_lis331.Rate.RATE_100_HZ
# lis.data_rate = adafruit_lis331.Rate.RATE_LOWPOWER_0_5_HZ

# enable the high pass filter without a reference or offset
lis.enable_hpf(True, cutoff=adafruit_lis331.RateDivisor.ODR_DIV_50, use_reference=False)

'''
# Dump one, because it shows too big number
lis.acceleration
time.sleep(0.01)
while time.time() - start_time < 1:
    x,y,z = lis.acceleration
    max_value_x = max(max_value_x, x)
    max_value_y = max(max_value_y, y)
    max_value_z = max(max_value_z, z)
    
    print(max_value_x,'\t',x)
    time.sleep(0.01)
'''